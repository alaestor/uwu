#define WINVER _WIN32_WINNT_WIN7 // windows vista can fuck off
#define WIN32_LEAN_AND_MEAN // windows junk can fuck off
#define NO_STRICT // windows types can fuck off

#include <iostream>
#include <stdexcept>
#include <windows.h>
#include "../hdr/windowcast.h"
#include "../hdr/keyboardmanager.h"


// WindowCast method utility objects


class ForegroundWindow
{
	const void* fgw;

	public:
	[[nodiscard]] operator const void*() const { return fgw; }
	[[nodiscard]] operator void*(){ return const_cast< void* >(fgw); }

	ForegroundWindow()
	: fgw(GetForegroundWindow())
	{
		if (fgw == nullptr) throw std::runtime_error(
			"Couldn't obtain foreground window");

		if constexpr (verbose) std::cout
			<< "\tForeground window obtained" << std::endl;
	}
};

typedef std::pair<long, long> Coordinate;
typedef Coordinate Dimensions;

class MonitorInfoFromWindow
{
	typedef MONITORINFOEXA MonitorInfo_t;

	[[nodiscard]] auto check(const void* hwnd)
	{
		if (hwnd == nullptr) throw std::invalid_argument("hwnd was null!");
		else return const_cast< void* >(hwnd);
	}

	[[nodiscard]] MonitorInfo_t getMonitorInfo()
	{
		MonitorInfo_t monitorInfo{};
		monitorInfo.cbSize = sizeof(MonitorInfo_t);
		if (monitor == nullptr) throw std::runtime_error(
			"MonitorFromWindow returned null");

		GetMonitorInfoA(const_cast< void* >(monitor), &monitorInfo);

		return monitorInfo;
	}

	public:
	const void* monitor;
	const MonitorInfo_t info;
	const decltype(MonitorInfo_t::szDevice)& name;
	Coordinate origin;
	Dimensions workspaceDimensions;

	MonitorInfoFromWindow(const void* hwnd) :
		monitor(MonitorFromWindow(check(hwnd), MONITOR_DEFAULTTONEAREST)),
		info(getMonitorInfo()),
		name(info.szDevice),
		origin(info.rcMonitor.left, info.rcMonitor.top),
		workspaceDimensions(
			info.rcWork.right - info.rcWork.left,
			info.rcWork.bottom - info.rcWork.top)
	{
		if constexpr (verbose) std::cout
			<< "\ton \" " << name << " \""
			<< " with dimensions "
			<< workspaceDimensions.first << "x" << workspaceDimensions.second
			<< " at origin "
			<< '(' << origin.first << ',' << origin.second << ')'
			<< std::endl;
	}
};

void moveWindow(const void* fgw, const Coordinate& orig, const Dimensions& dim)
{
	const auto& [dimension_x, dimension_y]{ dim };
	const auto& [origin_x, origin_y]{ orig };
	if (!SetWindowPos(
			const_cast< void* >(fgw),
			HWND_TOP, // it seems HWND_TOPMOST is "always ontop"
			origin_x, origin_y,
			dimension_x, dimension_y,
			SWP_SHOWWINDOW)) throw std::runtime_error(
		"Couldn't set window position");

	if constexpr (verbose) std::cout
		<< "\tmoved to origin "
		<< '(' << origin_x << ',' << origin_y << ')'
		<< " with dimensions "
		<< dimension_x << "x" << dimension_y
		<< std::endl;
}


// WindowCast methods

enum class pos_ns::Pos
{
	Top = (1 << 0),
	Bottom = (1 << 1),
	Left = (1 << 2),
	Right = (1 << 3)
};

[[nodiscard]] pos_ns::Pos operator|(pos_ns::Pos a, pos_ns::Pos b)
{ return pos_ns::Pos( int( a ) | int( b ) ); }

[[nodiscard]] bool operator&(pos_ns::Pos a, pos_ns::Pos b)
{ return (int( a ) & int( b )) != 0; }

void WindowCast::position(Pos p)
{
	struct s
	{
		const bool top:1;
		const bool bottom:1;
		const bool left:1;
		const bool right:1;
		const bool isBottom:1;
		const bool isRight:1;
		const bool isVertical:1;
		const bool isHorizontal:1;

		s(Pos p) :
			top(p & Pos::Top),
			bottom(p & Pos::Bottom),
			left(p & Pos::Left),
			right(p & Pos::Right),
			isBottom(bottom),
			isRight(right),
			isVertical(top || bottom),
			isHorizontal(left || right)
		{}
	} s(p);

	if ((s.top && s.bottom) || (s.left && s.right)) throw
		std::invalid_argument("WindowCast::position() Conflicting arguments");

	ForegroundWindow fgw;
	MonitorInfoFromWindow info(fgw);

	auto& origin{ info.origin };
	auto& [origin_x, origin_y]{ origin };
	auto& dimensions{ info.workspaceDimensions };
	auto& [dimension_x, dimension_y]{ dimensions };

	if (s.isBottom) origin_y += dimension_y / 2;
	if (s.isRight) origin_x += dimension_x / 2;
	if (s.isVertical) dimension_y /= 2;
	if (s.isHorizontal) dimension_x /= 2;

	moveWindow(fgw, origin, dimensions);
}

void WindowCast::toggleMaximize()
{
	ForegroundWindow fgw;
	WINDOWPLACEMENT wp{};
	wp.length = sizeof(wp);
	if (!GetWindowPlacement(fgw, &wp)) throw std::runtime_error(
		"Couldn't get window placement");

	wp.showCmd == SW_MAXIMIZE || wp.showCmd == SW_SHOWMAXIMIZED ?
	ShowWindow(fgw, SW_RESTORE) : ShowWindow(fgw, SW_SHOWMAXIMIZED);

	if constexpr (verbose) std::cout << '\t' <<
		(wp.showCmd == SW_MAXIMIZE || wp.showCmd == SW_SHOWMAXIMIZED ?
			"restored" : "maximized") << " window" << std::endl;
}

void WindowCast::center()
{
	ForegroundWindow fgw;
	MonitorInfoFromWindow info(fgw);

	auto& origin{ info.origin };
	auto& [origin_x, origin_y]{ origin };

	auto& dimensions{ info.workspaceDimensions };
	auto& [dimension_x, dimension_y]{ dimensions };

	origin_x += dimension_x / 3;
	origin_y += dimension_y / 3;
	dimension_x = 800;
	dimension_y = 600;

	moveWindow(fgw, origin, dimensions);
}

void WindowCast::test()
{
	if constexpr (verbose)
	{
		ForegroundWindow fgw;
		char t[MAX_PATH]{ '\0' };
		GetWindowText(fgw, t, sizeof(t));
		std::cout << "\tnamed \" " << t << " \"" << std::endl;
		MonitorInfoFromWindow info(fgw);
	}
}

bool WindowCast::isNumpadNum(unsigned char vkey)
{ return vkey > VK_NUMPAD0 - 1 && vkey < VK_NUMPAD9 + 1; }

bool WindowCast::inputEvent(
	[[maybe_unused]] const unsigned char vkey_in,
	[[maybe_unused]] const bool state,
	[[maybe_unused]] const KeyboardManager::modifier_s& modifiers)
{
	if (modifiers.win && state
		&& (isNumpadNum(vkey_in)
			|| vkey_in == VK_DECIMAL
			|| vkey_in == VK_OEM_PERIOD))
	{
		switch (vkey_in)
		{
		case VK_NUMPAD0: center(); break;
		case VK_NUMPAD1: position(Pos::Bottom | Pos::Left); break;
		case VK_NUMPAD2: position(Pos::Bottom); break;
		case VK_NUMPAD3: position(Pos::Bottom | Pos::Right); break;
		case VK_NUMPAD4: position(Pos::Left); break;
		case VK_NUMPAD5: toggleMaximize(); break;
		case VK_NUMPAD6: position(Pos::Right); break;
		case VK_NUMPAD7: position(Pos::Top | Pos::Left); break;
		case VK_NUMPAD8: position(Pos::Top); break;
		case VK_NUMPAD9: position(Pos::Top | Pos::Right); break;
		case VK_DECIMAL:
		case VK_OEM_PERIOD: test(); break;
		default: throw std::runtime_error(
			"WindowCast::inputEvent() unexpected default case");
		}
		return true;
	} else return false;
}
