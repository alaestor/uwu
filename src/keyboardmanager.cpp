#define WINVER _WIN32_WINNT_WIN7
#define WIN32_LEAN_AND_MEAN
#define NO_STRICT // FUCK OFF WINDOWS

#include <stdexcept>
#include <windows.h>
#include "../hdr/keyboardmanager.h"
#include "../hdr/remapper.h"
#include "../hdr/windowcast.h"
#include "../hdr/ramflusher.h"


// static data members


decltype(KeyboardManager::m_callbacks) KeyboardManager::m_callbacks;


// static methods


void KeyboardManager::registerInputCallback(kbm_fptr callback)
{ m_callbacks.push_back(callback); }

bool KeyboardManager::isValidKey(const unsigned char vkey)
{ return vkey != 0 && vkey <= VK_OEM_CLEAR; }

bool KeyboardManager::simulate_keystroke(
	const unsigned char vkey_out, const bool downPress)
{
	if (!isValidKey(vkey_out)) return false;
	INPUT in;
	in.type = INPUT_KEYBOARD;
	in.ki.wVk = vkey_out;
	in.ki.dwFlags = downPress ? 0 : KEYEVENTF_KEYUP;
	in.ki.dwExtraInfo = 0;
	in.ki.time = 0;
	in.ki.wScan = static_cast< decltype(in.ki.wScan) >(
		MapVirtualKey(vkey_out, MAPVK_VK_TO_VSC));

	return SendInput(1, &in, sizeof(in)) != 0;
}

long long __stdcall KeyboardManager::llKeyboardProc(
	int nCode,
	long long unsigned int wParam,
	long long int lParam)
{ // low-level keyboard callback
	static modifier_s modifier;

	if (nCode == HC_ACTION)
	{
		KBDLLHOOKSTRUCT& kb{ *reinterpret_cast< KBDLLHOOKSTRUCT* >(lParam) };
		const unsigned char vkey{ static_cast< unsigned char >(kb.vkCode) };
		const bool state{ wParam == WM_KEYDOWN };

		switch (vkey) // set modifier state
		{
		case VK_LSHIFT:
		case VK_RSHIFT:
		case VK_SHIFT: modifier.shift = state; break;
		case VK_LCONTROL:
		case VK_RCONTROL:
		case VK_CONTROL: modifier.ctrl = state; break;
		case VK_LMENU:
		case VK_RMENU:
		case VK_MENU: modifier.alt = state; break;
		case VK_LWIN:
		case VK_RWIN: modifier.win = state; break;
		default: break;
		}

		if (modifier.win && state && vkey == VK_END)
			PostQuitMessage(0);

		bool supressInput{ false };

		try // behavior
		{
			for (const auto& callback : m_callbacks)
				supressInput |= callback(vkey, state, modifier);
		}
		catch (...) // TODO error handling. I'm lazy. Just swallow errors.
		{ std::terminate(); }

		if (supressInput) return -1;
	}
	return CallNextHookEx(nullptr, nCode, wParam, lParam);
}

void KeyboardManager::inputMsgLoop()
{
	// std::undefined_behavior - thanks windows :(
	typedef long long int(*fptr_t)();
	fptr_t callback;
	callback = reinterpret_cast< fptr_t >(
		reinterpret_cast< void* >(llKeyboardProc));

	class SafeHook
	{
		void* m_hndl;

		public:
		explicit operator void*() { return &m_hndl; }
		SafeHook(void* hhook) : m_hndl(hhook){};
		~SafeHook(){ UnhookWindowsHookEx(m_hndl); }
	} kbh(SetWindowsHookEx( WH_KEYBOARD_LL, callback, nullptr, 0 ));
	// nullptr used to be: GetModuleHandle("kernel32.dll")

	int r;
	MSG msg;
	while ((r = GetMessage(&msg, nullptr, 0, 0)) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	if (r == -1){ return; } // error, TODO maybe
}
