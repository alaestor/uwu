#define WINVER _WIN32_WINNT_WIN7
#define WIN32_LEAN_AND_MEAN
#define NO_STRICT // FUCK OFF WINDOWS

#include <iostream>
#include <string>
#include <windows.h>
#include "../hdr/keyboardmanager.h"
#include "../hdr/remapper.h"
#include "../hdr/windowcast.h"
#include "../hdr/ramflusher.h"
#include "../hdr/processkiller.h"

bool isCurrentProcessIsElevated()
{ // taken from FGL standard windows header
	auto h{ INVALID_HANDLE_VALUE };
	TOKEN_ELEVATION te;
	if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &h))
	{
		if (h == INVALID_HANDLE_VALUE) return false;
		unsigned long cbSize{ sizeof(TOKEN_ELEVATION) };
		GetTokenInformation(h, TokenElevation, &te, sizeof(te), &cbSize);
		CloseHandle(h);
	}
	return te.TokenIsElevated ? true : false;
}

auto remap_keys()
{
	KeyRemapper remap;
	remap.add(VK_F20, VK_NUMPAD7);
	remap.add(VK_F21, VK_NUMPAD8);
	remap.add(VK_F24, VK_NUMPAD9);
}

auto print_hotkeys()
{
	// hardcoded for now
	std::cout
		<< "\tuwu whats this?\n"
		<< "\tuwu hotkeys!\n"
		<< "\t---\n"
		<< "\tRemaps: [F20]->[np7], [F21]->[np8], [F24]->[np9]\n"
		<< "\t---\n"
		<< "\t[Win + F4]\tor [Alt + F4] will close stuborn programs\n"
		<< "\t[Win + .]\tprints debug info when in verbose mode\n"
		<< "\t[Win + -]\twill flush non-critical RAM allocations to pagefile\n"
		<< "\t[Win + Num5]\twill toggle window maximization\n"
		<< "\t[Win + Num0]\tcenters and resizes current window\n"
		<< "\t[Win + Num#]\tmoves and resizes the selected window\n"
		<< "\t[Win + End]\texit\n"
		<< "\t---\n"
		<< std::endl;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
	if (!isCurrentProcessIsElevated()) std::cout <<
		"\tWARNING: Must be run as administrator to work properly!"
		<< std::endl;

	std::cout << "\n\t!!! Ultra Windows7 Utility !!!\n" << std::endl;

	remap_keys();

	using KMB = KeyboardManager;
	KMB::registerInputCallback(KeyRemapper::inputEvent);
	KMB::registerInputCallback(WindowCast::inputEvent);
	KMB::registerInputCallback(RamFlusher::inputEvent);
	KMB::registerInputCallback(ProcessKiller::inputEvent);

	print_hotkeys();
	std::cout << "\tuwu is fully operational" << std::endl;

	KeyboardManager::inputMsgLoop();
	// could be a detached thread if we have more to do

	std::cout << "\n\n\tuwu Goodbye World..." << std::endl;
	return 0;
}

