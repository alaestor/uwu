#define WINVER _WIN32_WINNT_WIN7
#define WIN32_LEAN_AND_MEAN
#define NO_STRICT // FUCK OFF WINDOWS

#include <stdexcept>
#include <windows.h>
#include "../hdr/processkiller.h"
#include "../hdr/keyboardmanager.h"


// ProcessHandle methods


bool ProcessKiller::ProcessHandle::isWIN32NT() const noexcept
{
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(osv);
	GetVersionEx(&osv);
	return osv.dwPlatformId == VER_PLATFORM_WIN32_NT;
}

struct ProcessKiller::ProcessHandle::PrivInfo
	ProcessKiller::ProcessHandle::getDebugPrivilage() const
{
	const unsigned long tokens{ TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES };
	PrivInfo pi;
	if (!OpenThreadToken(GetCurrentThread(), tokens, false, &pi.hToken))
	{
		if (GetLastError() != ERROR_NO_TOKEN
			|| !OpenProcessToken( GetCurrentProcess(), tokens, &pi.hToken))
				throw std::runtime_error(
					"ProcessHandle: Failed to get thread token handle");
	}

	TOKEN_PRIVILEGES Priv;
	Priv.PrivilegeCount = 1;
	Priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	unsigned long cbPriv{ sizeof(Priv) };

	LookupPrivilegeValue(nullptr, SE_DEBUG_NAME, &Priv.Privileges[0].Luid);

	// try to enable the privilege
	if (!AdjustTokenPrivileges( pi.hToken, false,
		&Priv, sizeof(Priv), &pi.PrivOld, &cbPriv))
	{
		CloseHandle(pi.hToken);
		throw std::runtime_error("ProcessHandle: AdjustTokenPrivs failed");
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)
	{
		CloseHandle(pi.hToken);
		throw std::runtime_error("ProcessHandle: SE_DEBUG_NAME failed");
	}

	return pi;
}

void ProcessKiller::ProcessHandle::resetPrivilage(struct PrivInfo& pi)
	const noexcept
{
	AdjustTokenPrivileges(
		pi.hToken, false, &pi.PrivOld, sizeof(pi.PrivOld), nullptr, nullptr);
	CloseHandle(pi.hToken);
}

void* ProcessKiller::ProcessHandle::getHandle(unsigned long pid) const
{
	auto h{ OpenProcess(PROCESS_TERMINATE, false, pid) };
	if (h == nullptr)
	{
		if (GetLastError() != ERROR_ACCESS_DENIED) // derpy?
			throw std::runtime_error("ProcessHandle: ERROR_ACCESS_DENIED");

		if (!isWIN32NT()) // insane?
			throw std::runtime_error("ProcessHandle: isn't WIN32_NT");

		auto pi{ getDebugPrivilage() };
		h = OpenProcess(PROCESS_TERMINATE, false, pid);
		resetPrivilage(pi);

		if (h == nullptr) throw std::runtime_error(
			"ProcessHandle: OpenProcess failed");
	}
	return h;
}


// static methods


unsigned long ProcessKiller::getForegroundWindowPid()
{
	const auto& hwnd{ GetForegroundWindow() };
	if (hwnd == nullptr || hwnd == GetDesktopWindow())
		throw std::runtime_error("Invalid hwnd");
	unsigned long pid{ 0 };
	GetWindowThreadProcessId(hwnd, &pid);
	if (pid == 0) throw std::runtime_error("Invalid pid");
	return pid;
}

void ProcessKiller::killProcess(unsigned long pid)
{
	// should I build in safety to not nuke windows systems? meh.
	// should try softer methods & check using GetExitCodeProcess? meh.
	if (!TerminateProcess(ProcessHandle(pid), 0xDEAD))
		throw std::runtime_error("ProcessKiller::killProcess() Failed.");
}

bool ProcessKiller::inputEvent(
	const unsigned char vkey_in,
	const bool state,
	const KeyboardManager::modifier_s& modifiers)
{
	if ((modifiers.alt || modifiers.win) && state && vkey_in == VK_F4)
	{
		killProcess(getForegroundWindowPid());
		return true;
	}
	else return false;
}
