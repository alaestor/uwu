#define WINVER _WIN32_WINNT_WIN7
#define WIN32_LEAN_AND_MEAN
#define NO_STRICT // FUCK OFF WINDOWS

#include <array>
#include <windows.h>
#include "../hdr/remapper.h"
#include "../hdr/keyboardmanager.h"


// static data members


decltype(KeyRemapper::m_a) KeyRemapper::m_a = { 0 };


// methods


auto KeyRemapper::vKeyToIndex(const unsigned char vkey_in)
{ return static_cast< decltype(m_a)::size_type >(vkey_in - 1); }

bool KeyRemapper::inputEvent(
	[[maybe_unused]] const unsigned char vkey_in,
	[[maybe_unused]] const bool state,
	[[maybe_unused]] const KeyboardManager::modifier_s& modifiers)
{ // input behavior
	using kbm = KeyboardManager;
	if (const auto& k = m_a.at(vKeyToIndex(vkey_in)); kbm::isValidKey(k))
		return kbm::simulate_keystroke(k, state);
	else return false;
}

void KeyRemapper::add(
	const unsigned char vkey_in, const unsigned char vkey_out)
{ m_a.at(vKeyToIndex(vkey_in)) = vkey_out; }

void KeyRemapper::remove(const unsigned char vkey_in)
{ m_a.at(vKeyToIndex(vkey_in)) = 0; }

void KeyRemapper::removeAll()
{ for (auto& i : m_a) i = 0; }
