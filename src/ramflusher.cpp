#define WINVER _WIN32_WINNT_WIN7
#define WIN32_LEAN_AND_MEAN
#define NO_STRICT // FUCK OFF WINDOWS

#include <stdexcept>
#include <vector>
#include <windows.h>
#include <psapi.h>
#include "../hdr/ramflusher.h"
#include "../hdr/keyboardmanager.h"

void RamFlusher::flush_silent()
{
	static constexpr const std::size_t MaxPIDs = 1024;
	std::vector<unsigned long> pids(MaxPIDs);
	pids.resize(MaxPIDs); // "theoretically" unneeded

	if (unsigned long bytesWriten = 0;
		EnumProcesses(
			pids.data(),
			static_cast<unsigned long>(pids.size()),
			&bytesWriten))
	{ pids.resize(bytesWriten / sizeof(decltype(pids)::value_type)); }
	else throw std::runtime_error("flush_all_silent() EnumProcesses() failed");

	static constexpr const unsigned long AccessFlags =
		PROCESS_QUERY_INFORMATION | PROCESS_SET_QUOTA | PROCESS_VM_READ;

	for (const unsigned long& pid : pids) // best effort
		if (const auto& h(OpenProcess(AccessFlags, false, pid)); h != nullptr)
		{ EmptyWorkingSet(h); CloseHandle(h); }
}

bool RamFlusher::inputEvent(
		const unsigned char vkey_in,
		const bool state,
		const KeyboardManager::modifier_s& modifiers)
{
	if (modifiers.win && state
		&& (vkey_in == VK_SUBTRACT || vkey_in == VK_OEM_MINUS))
	{
		flush_silent();
		return true;
	}
	else return false;
}
