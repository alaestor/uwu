# Ultra Windows7 Utility

*A.K.A.* **uwu** *because; why not.*

---

##### Cons
- Doesnt follow Windows conventions or style
- Quick and dirty
- Not optimal C++
- Not utilizing OOD, DDD, TDD, or polymorphism
- Little to no error handling (aka: the *shit bricks* protocol)

##### Pros
- Doesnt follow Windows conventions or style
- Runs fast
- Works fine for me, I don't care about you
- It's uwu

##  Features
- Ulta uwu
- Ultra ALT-F4 (*stops you from nuking desktop, but still dangerous*)
- Ultra Flush lets you page non-critical ram allocations
- Ultra Window Tiling is like win+arrowkeys, but better
- Ultra Key Remapper allows for hardcoded key remapping

## Keybinds
- Key Remaps (*hardcoded, see main.cpp*)
    - [**F20**]->[**numpad7**]
    - [**F21**]->[**numpad8**]
    - [**F24**]->[**numpad9**]
- [**Win + F4**]      or [**Alt + F4**] will close stuborn programs
- [**Win + .**]       prints debug info when in verbose mode (*todo*)
- [**Win + -**]       will flush non-critical RAM allocations to pagefile
- [**Win + Num5**]    will toggle window maximization
- [**Win + Num0**]    centers and resizes current window
- [**Win + Num#**]    moves and resizes the selected window
- [**Win + End**]     exit

## Download

Though I have no idea why you would want to because **it's entirely hardcoded**,

if you would like to download the pre-compiled and compressed *64-bit Windows7 Portable Executible*;

you may do so from **[mega.nz](https://mega.nz/#F!AwtThQQJ!lcHVZFAjH7jJ0DTgDELLZw)** - *normal* and *lite*(**\***) versions available

| **FILE** | **SIZE** | **CRC32** | **SHA-1** | **VirusTotal** |
| ------ | ------ | ------ | ------ | ------ |
| uwu.EXE | 827 KB | `9C16BC6D` | `E5B27422521AAC1D66058AC17E40CAA3E2BACB7F` | [00/71 Detections](https://www.virustotal.com/gui/file/69504fe16f6659c6860f15b166eb630689bcb2b0ec1d851c0325ab6dbfefb753/detection) |
| uwu.ZIP | 298 KB | `CB0EF75B` | `38C8FFE1F5828B6F1E7B1EB3ABCF1AA7A79F879D` | [00/63 Detections](https://www.virustotal.com/gui/file/abc07b84ad1dd2f92deec249a077e9343b8ae0c99fbb9f6adff07f776ad6b7b6/detection) |
| uwu-lite.EXE **\***  | 208 KB | `6058D637` | `47F032F2026154F55AA523A14918FE0B780B699B` | [04/67 Detections](https://www.virustotal.com/gui/file/5fede1278052bafe5de36c2eb017e7625947b79cc16aeaf6a5b18e3d32173b7b/detection) **\*\*** |
| uwu-lite.ZIP **\***  | 207 KB | `B12E3E6F` | `42F45C0960DDBFEC1AE968A97861FE254D5BDD2C` | [04/63 Detections](https://www.virustotal.com/gui/file/954bfb8019103d7c8429f7b72669ac8242ba254792889f3524727b64ca6fc014/detection) **\*\*** |


( **\*** ) `uwu-lite.exe` was packed using [UPX - the Ultimate Packer for eXecutables](https://upx.github.io) to reduce filesize.

  UPX commandline: `upx -k --best --ultra-brute --overlay=strip --compress-resources=0 --overlay=strip`

( **\*\*** ) The packing and compression options may result in false-positive reports from some security software.

