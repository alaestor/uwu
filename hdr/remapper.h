#ifndef REMAPPER_H_INCLUDED
#define REMAPPER_H_INCLUDED

#define WINVER _WIN32_WINNT_WIN7
#define WIN32_LEAN_AND_MEAN
#define NO_STRICT // FUCK OFF WINDOWS

#include <limits>
#include <array>
#include <windows.h>
#include "../hdr/keyboardmanager.h"

class KeyRemapper
{
	private:
	static std::array<
		unsigned char, std::numeric_limits<unsigned char>::max()> m_a;

	static auto vKeyToIndex(const unsigned char vkey_in);

	public:
	static bool inputEvent(
		const unsigned char vkey_in,
		const bool state,
		const KeyboardManager::modifier_s& modifiers);

	void add(const unsigned char vkey_in, const unsigned char vkey_out);
	void remove(const unsigned char vkey_in);
	void removeAll();
};

#endif // REMAPPER_H_INCLUDED
