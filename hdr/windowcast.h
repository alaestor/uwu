#ifndef WINDOWCAST_H_INCLUDED
#define WINDOWCAST_H_INCLUDED

/*
	2018 Window Cast
http://discord.futuregadgetlab.net

full multi-monitor support

HOTKEYS:
[winkey + numpad 1~9] to move and resize selected window
[winkey + numpad 5] to toggle maximizing window
[winkey + numpad 0] bring window to middle resized 800x600
[winkey + period] to print some window debug info when verbose is true

set "verbose" to false to avoid spam and disable period debug
*/

#include "../hdr/keyboardmanager.h"

static constexpr const bool verbose{ false };

namespace pos_ns { enum class Pos; }

class WindowCast
{
	private:
	using Pos = pos_ns::Pos;
	// modify foreground window
	static void position(Pos p);
	static void toggleMaximize();
	static void center();
	static void test();

	public:
	static bool isNumpadNum(unsigned char vkey);
	static bool inputEvent(
		const unsigned char vkey_in,
		const bool state,
		const KeyboardManager::modifier_s& modifiers);
};

#endif // WINDOWCAST_H_INCLUDED
