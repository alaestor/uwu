#ifndef PROCESSKILLER_H_INCLUDED
#define PROCESSKILLER_H_INCLUDED

#define WINVER _WIN32_WINNT_WIN7 // windows vista can fuck off
#define WIN32_LEAN_AND_MEAN // windows junk can fuck off
#define NO_STRICT // windows types can fuck off

#include <windows.h>
#include "../hdr/keyboardmanager.h"

class ProcessKiller
{
	class ProcessHandle
	{
		private:
		void* hndl;
		struct PrivInfo
		{
			void* hToken;
			TOKEN_PRIVILEGES PrivOld;
		};
		bool isWIN32NT() const noexcept;
		struct PrivInfo getDebugPrivilage() const;
		void resetPrivilage(struct PrivInfo& pi) const noexcept;
		void* getHandle(unsigned long pid) const;

		public:
		operator HANDLE() const { return reinterpret_cast<HANDLE>(hndl); }
		ProcessHandle(unsigned long pid) : hndl(getHandle(pid)) {}
		~ProcessHandle() { CloseHandle(hndl); }
	};

	static unsigned long getForegroundWindowPid();
	static void killProcess(unsigned long pid);

	public:
	static bool inputEvent(
		const unsigned char vkey_in,
		const bool state,
		const KeyboardManager::modifier_s& modifiers);
};

#endif // PROCESSKILLER_H_INCLUDED
