#ifndef RAMFLUSHER_H_INCLUDED
#define RAMFLUSHER_H_INCLUDED

#include "../hdr/keyboardmanager.h"

class RamFlusher
{
	private:
	static void flush_silent();
	public:
	static bool inputEvent(
		const unsigned char vkey_in,
		const bool state,
		const KeyboardManager::modifier_s& modifiers);
};

#endif // RAMFLUSHER_H_INCLUDED
