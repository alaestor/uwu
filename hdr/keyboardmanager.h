#ifndef KEYBOARDMANAGER_H_INCLUDED
#define KEYBOARDMANAGER_H_INCLUDED

#define WINVER _WIN32_WINNT_WIN7
#define WIN32_LEAN_AND_MEAN
#define NO_STRICT // FUCK OFF WINDOWS

#include <limits>
#include <vector>
#include <windows.h>

class KeyboardManager
{
	public:
	struct modifier_s
	{
		bool shift:1;
		bool ctrl:1;
		bool alt:1;
		bool win:1;

		bool isDown(){ return shift || ctrl || alt || win; }
		modifier_s() : shift(false), ctrl(false), alt(false), win(false) {}

		private:
		bool padding:4;
	};

	typedef bool (*kbm_fptr)(
		[[maybe_unused]] const unsigned char vkey_in,
		[[maybe_unused]] const bool state,
		[[maybe_unused]] const modifier_s& modifiers);

	static void registerInputCallback(kbm_fptr callback);
	static bool isValidKey(const unsigned char vkey);
	static bool simulate_keystroke(
		const unsigned char vkey_out, const bool downPress);
	static void inputMsgLoop(); // registers llkeyboardproc callback and loops

	private:
	static std::vector<kbm_fptr> m_callbacks;

	// handles all low-level kb input events
	// will call functions registered with registerInputCallback()
	static long long __stdcall llKeyboardProc(
		int nCode,
		long long unsigned int wParam,
		long long int lParam);

};

#endif // KEYBOARDMANAGER_H_INCLUDED

